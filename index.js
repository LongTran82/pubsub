// subscriber store
let topics = {}
// example:
// topics = {topic1: [subscriber1, subscriber2], topic2: [subscriber3]}


let subscribe = (topic, subscriber) => {
    if (!topics[topic])
    topics[topic]=[]
  // initializes topic if not existent
  // push subscriber to topic's listener
    topics[topic].push(subscriber)
  //  console.log(topics)
}

let publish = (topic, option) => {
  // for each registered subscriber to given topic, call subscriber(option)
  topics[topic].forEach(sub => sub(obtion))
  // console.log(`topic = ${topic`})
  // console.log(`option = ${option}`)
}
module.exports = { subscribe: subscribe, publish: publish }