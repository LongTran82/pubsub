let bus = require('./index')

function a(question){
  if (question)
    bus.publish('question', question)
}

function b(){
  return 42
}

bus.subscribe('question', (question) => console.log(b(question)))
bus.subscribe('question', () => console.log('ça dépend'))

a("Quel est le sens de la vie, de l'univers et le reste ?")